/*
 * onboard_modem_api.c
 *
 *  Created on: 24 ����. 2018 �.
 *      Author: Denis Shreiber
 */

#if MBED_CONF_NSAPI_PRESENT

#include "onboard_modem_api.h"
#include "gpio_api.h"
#include "platform/mbed_wait_api.h"
#include "PinNames.h"

#if MODEM_ON_BOARD

static void power_on() {

}

static void power_off() {

}

// Note microseconds not milliseconds
static void press_power_button(int time_us)
{
//    gpio_t gpio;
//
//    gpio_init_out_ex(&gpio, MDMPWRON, 0);
//
//    wait_us(time_us);
//    gpio_write(&gpio, 1);
}

void onboard_modem_init()
{
//    gpio_t gpio;
//
//    // Take us out of reset
//    gpio_init_out_ex(&gpio, MDMRST,    1);
}

void onboard_modem_deinit()
{
//    gpio_t gpio;
//
//    // Back into reset
//    gpio_init_out_ex(&gpio, MDMRST, 0);
}

void onboard_modem_power_up()
{
//#if defined(TARGET_UBLOX_C030_R410M)
//	/* keep the power line low for 1 seconds */
//    press_power_button(1000000);
//#else
//	/* keep the power line low for 50 microseconds */
//    press_power_button(50);
//#endif
//
//    /* give modem a little time to respond */
//    wait_ms(100);
}

void onboard_modem_power_down()
{
//#if defined(TARGET_UBLOX_C030_R410M)
//	/* keep the power line low for 1.5 seconds */
//    press_power_button(1500000);
//#else
//	/* keep the power line low for 1 seconds */
//    press_power_button(1000000);
//#endif
}

#endif //MODEM_ON_BOARD
#endif //MBED_CONF_NSAPI_PRESENT



