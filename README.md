# GPS Terminal source code

(Forked from [Example cellular application for Mbed OS](https://github.com/ARMmbed/mbed-os-example-cellular))

## ��������� ��

### Eclipse

������� ��������� ����� [Eclipse IDE for C/C++ Developers](http://www.eclipse.org/downloads/packages/).

### GCC ARM

���������� ���������� [GCC ARM](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads).

�� ������ ��������� ���������� ��� ������ Mbed CLI ���������� ������ GCC ARM: **6.0.0 < version < 7.0.0**. ���� ����� ������ ������, Mbed CLI ���� �������.

*����������. GCC ARM ����� ���������� ��� ��������� Mbed CLI.*

### OpenOCD

��� ������� ��������� �������� [OpenOCD](https://github.com/gnu-mcu-eclipse/openocd/releases).

������� � ����������� � ����� *ARM Tools\GNU MCU Eclipse\OpenOCD*.

### Windows Build Tools

� Windows ��� ������ ������� ��������� unix-������� [make � rm](https://github.com/gnu-mcu-eclipse/windows-build-tools/releases).

������� � ����������� � ����� *ARM Tools\GNU MCU Eclipse\Build Tools*.

### Git

���������� ��������� [Git](https://git-scm.com/).

*����������. Git ����� ���������� ��� ��������� Mbed CLI.*

### Mbed CLI

��� ������� ������ � Mbed OS ���������� ������� [Mbed CLI](https://os.mbed.com/docs/latest/tools/installing-with-the-windows-installer.html).

## ��������� ���������

### GNU MCU Eclipse plugin

��������� Eclipse. � *Eclipse Marketplace* ����� � ���������� ������ *GNU MCU Eclipse*. [�������� �������](https://gnu-mcu-eclipse.github.io/)

### ����

� ���������� Eclipse *Window -> Preferences -> MCU

- ��� *Global ARM Toolchains Paths* ������� ���� �� *"C:\Program Files (x86)\GNU Tools ARM Embedded\6 2017-q2-update\bin"*
- ��� *Global Build Tools Path* ������� ���� �� *"D:\ARMTools\GNU ARM Eclipse\Build Tools\2.11-20180428-1604\bin"*
- ��� *Global OpenOCD Paths* ������� ���� �� *"D:\ARMTools\GNU ARM Eclipse\OpenOCD\0.10.0-8-20180512-1921\bin"*

### ������ ���������

��� �������� ������� � ��������� ��������� ��������� ����������� ���������� �������� ������ ARM ��������� � ������������ Keil.

- � Eclipse ������� ����������� *Packs* (��������, �������� �� ������ *Make the C/C++ Packs perspective visible*).
- �� ������� *Packs* ������ *�������� �� ���� ������������*. ���������� �������� ��������������� �����.
- ���������� ����� ��� ������� �����������, ������� ������ ������� ����

## ������ �������

### ����� Eclipse

File -> Import -> Git -> Projects from Git -> Clone URI

- ������ ����������, ������ ����� �����������, ��������������� ������ � ��.:
    - SSH: git@bitbucket.org:syberdevices/gps-terminal.git
    - HTTPS: https://denis_shreiber@bitbucket.org/syberdevices/gps-terminal.git
- ���� � �������� ����� ���������� ���� ������� ����� eclipse-workspace, �� ������������� ��� existing Eclipse projects

### ����� SourceTree

...

## ���������� ����������� ���������� Mbed OS

Debug:

```mbed compile -t GCC_ARM -m GPS_TERMINAL_CUSTOM_BOARD --library --source=mbed-os --source=custom_targets --build=static-libs/mbed-os/debug --profile mbed-os/tools/profiles/debug.json```
    
Develop:

```mbed compile -t GCC_ARM -m GPS_TERMINAL_CUSTOM_BOARD --library --source=mbed-os --source=custom_targets --build=static-libs/mbed-os/develop --profile mbed-os/tools/profiles/develop.json```
    
Release:

```mbed compile -t GCC_ARM -m GPS_TERMINAL_CUSTOM_BOARD --library --source=mbed-os --source=custom_targets --build=static-libs/mbed-os/release --profile mbed-os/tools/profiles/release.json```

## ������ Launch Configuration

����������� ��������� ������������ ������� ���������:

File -> Import -> Run/Debug -> Launch Configurations

� ����� ������� ������� ���� *gps-terminal Debug.launch*

## Git � Eclipse

������ � Git � Eclipse ������� � ���� ������������:

- *Git* - ������� � ������������� � ���������� �������������
- *Team Synchronizing* - ��������� ��������� ������������ � �������������

������� ����������� �����:

Window -> Perspective -> Open Perspective




